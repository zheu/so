<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Administrator
 */
class Administrator extends Model
{
    protected $table = 'administrators';

    public $timestamps = true;

    protected $fillable = [
        'username',
        'password',
        'name',
        'remember_token'
    ];

    protected $guarded = [];

        
}