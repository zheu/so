<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Song
 */
class Song extends Model
{
    protected $table = 'songs';
    public $timestamps = true;
    protected $fillable = [ 'id', 'artist_id', 'name', 'text' ];
    protected $guarded = [];

    public static function getList() {
        return static::lists('name', 'id')->all();
    }
    public function artist() {
        return $this->belongsTo(\App\Models\Artist::class);
    }


}