<?php

Admin::model(App\Models\Song::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{

})->form(function ()
{
	FormItem::select('artist_id', 'Artist')->list(Artist::class);
	FormItem::text('name', 'Name');
	FormItem::ckeditor('text', 'Text');
});