<?php

Admin::model(App\Models\Administrator::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::string('username', 'login');
  Column::string('name', 'имя');  

})->form(function ()
{
	FormItem::text('username', 'Username');
	FormItem::text('password', 'Password');
	FormItem::text('name', 'Name');
	FormItem::text('remember_token', 'Remember Token');
});