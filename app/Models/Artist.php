<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Artist
 */
class Artist extends Model
{
    protected $table = 'artists';
    public $timestamps = true;
    protected $fillable = [ 'id', 'name', 'info'  ];
    protected $guarded = [];

    public function songs() {
        return $this->hasMany(\App\Models\Song::class);
    }        

}